#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

#define BUFSIZE 512
enum {
  CONS,
  NUM,
  SYM,
  T_LBRACE,
  T_RBRACE
};

typedef struct {
  void *v;
  int t;
  void *n;
} ast_t;

int repl(char *);
ast_t *tokenize(char **s);
ast_t *getnum(char **s);
ast_t *getsym(char **s);
ast_t *read(char *s);
int pprint(ast_t *n);

int main(int argc, char **argv)
{
  char *input = malloc(sizeof(char)*BUFSIZE);
  return repl(input);
}

int repl(char *s)
{
  void *ast;
  while(1) {
    printf("clisp> ");
    ast = read(s);
    if(!ast) break;
    /* ast = eval(ast); */
    printf("=> ");
    pprint(ast);
  }
  free(s);
  return 0;
}

/* we have to use a pointer to a char, because we want the position in
 * the string to update without too much fuss */
ast_t *tokenize(char **s) {
  ast_t *n;
  while((*s)++)
  {
    if(isspace(**s)) { continue; }
    if(!isdigit(*((*s)-1)) && **s != '(' && **s != ')') {
      if(!isspace(*((*s)-1)))
        (*s)--;
      n = getsym(s);
      break;
    }
    else if(isdigit(**s)) {
      if(!isspace(*((*s)-1)))
        (*s)--;
      n = getnum(s);
      break;
    }
    else if(**s == '(')
      continue;
      /* n->t = CONS; */
      /* n->v = malloc(sizeof(ast_t)); */
      /* n-> */
  }
  return n;
}

ast_t *getnum(char **s)
{ /* I... I'm sorry... */
  ast_t *tp = malloc(sizeof(ast_t));
  int i; char *tmp;
  for(i = 0; isdigit(**s) && !isspace(**s); i++)
    (*s)++;
  tmp = malloc(sizeof(char)*(i+1));
  strncpy(tmp, *s-i, i);
  if(!(atoi(tmp))) {
    printf("ERROR. Input: %s\n", *s-i);
    free(tmp); free(tp);
    exit(-1); /* yay possibly dangerous behaviour */
  }
  tp->v = malloc(sizeof(int));
  *((int *)tp->v) = atoi(tmp);
  free(tmp);

  tp->t = NUM; tp->n = NULL;
  return tp;
}

ast_t *getsym(char **s)
{
  ast_t *tp = malloc(sizeof(ast_t));
  int i; size_t len = strlen(*s);
  for(i = 0; i <= len && !isspace(**s); i++)
    (*s)++;
  tp->v = malloc(sizeof(char)*(i+1));
  strncpy(((char *)tp->v), *s-i, i);

  tp->t = SYM; tp->n = NULL;
  return tp;
}

ast_t *read(char *s)
{
  if(!fgets(s, BUFSIZE, stdin))
    return NULL;
  if(*s == '.') return NULL;
  ast_t *tp = tokenize(&s);
  pprint(tp);
  ast_t *tp0 = tokenize(&s);
  pprint(tp0);
  return tp;
}

int pprint(ast_t *n)
{
  if(n->t == SYM)
    printf("%s\n", (char *)n->v);
  else if(n->t == NUM)
    printf("%d\n", *((int *)n->v));
  return 0;
}







